var ignoreQueryLog = {
	callMeth:{
		searchTrackQuery:"searchTrackQuery",
		ignoreQuery:'ignoreQuery'
	},
	init:function(){
		this.bind();
	},
	bind:function(){
		var curo = this;
		//绑定搜索按钮
		$("#searchBtn").click(function(){
			curo.ignoreQuery();
		});
	},
	//查询
	ignoreQuery:function(){
		$("#ignoreGrid").datagrid('load',{
			methName: this.callMeth.ignoreQuery,
			tSearchDataStart: $("#tSearchDataStart").val(),
			tSearchDataEnd: $("#tSearchDataEnd").val()
		});
	},
	//搜索轨迹
	searchTrack:{
		showTrackWin:function(searchSessionId){
			this.doQuery(searchSessionId);
			$("#searchTrackWin").window('open');
		},
		doQuery:function(searchSessionId){
			$.post("queryLog.do",{
				methName:ignoreQueryLog.callMeth.searchTrackQuery,
				searchSessionId:searchSessionId
			},function(data){
				if(data && data.success){
					$("#searchTrackGrid").datagrid({
						data: data.rows
					});
				}
			},"json");
			
		}
	}
};

$(function(){
	ignoreQueryLog.init();
});

function formatSearchTrack(val,row,index){  
    return '<a href="javascript:void(0);" onclick="ignoreQueryLog.searchTrack.showTrackWin(\''+row.searchSessionId+'\')">' + val + '</a>';  
}
