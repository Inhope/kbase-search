var queryLog = {
	callMeth:{
		doQuery:'doQuery',
		ignore:'ignore',
		searchTrackQuery:"searchTrackQuery",
		addInExpandQuestion:"addInExpandQuestion",
		signRatio:"signRatio"
	},
	init:function(){
		this.bind();
		this.signRatio();
	},
	bind:function(){
		var curo = this;
		//绑定搜索按钮
		$("#searchBtn").click(function(){
			curo.doQuery();
		});
	},
	/**
	 * 查询，同时统计签读的点击率
	 */
	doQuery:function(){
		$("#queryLogGrid").datagrid('load',{
			methName: queryLog.callMeth.doQuery,
			tSearchTrackCountOneExpr:$("#tSearchTrackCountOneExpr").combobox("getValue"),
			tSearchTrackCountTwoExpr:$("#tSearchTrackCountTwoExpr").combobox("getValue"),
			tSearchTrackCountOne:$("#tSearchTrackCountOne").numberbox('getValue'),
			tSearchTrackCountTwo:$("#tSearchTrackCountTwo").numberbox('getValue'),
			tSearchDataStart: $("#tSearchDataStart").val(),
			tSearchDataEnd: $("#tSearchDataEnd").val()
		});
	},
	/**
	 * 签读点击率
	 */
	signRatio:function(){
		$.post("queryLog.do",{
			methName:this.callMeth.signRatio
		},function(data){
			if(data && data.success){
				$("#signRatio").text("点击率:" + data.message);
			}
		},"json");
	},
	/**
	 * 忽略
	 */
	ignore:function(searchSessionId){
		$.post("queryLog.do",{
			methName:this.callMeth.ignore,
			searchSessionId:searchSessionId
		},function(data){
			if(data && data.success){
				$("#queryLogGrid").datagrid("reload");
			}
		},"json");
	},
	/**
	 * 将提问文本添加到最后点击或者签读的qa中，优先添加到签读中
	 */
	addInExpandQuestion:function(searchSessionId,lastQuestionId,signQuestionId,extendQuestion){
		if(!lastQuestionId && !signQuestionId){
			$.messager.alert('警告','提问文本没有关联的知识，不能执行该操作'); 
			return;
		}
		
		$.post("queryLog.do",{
			methName:this.callMeth.addInExpandQuestion,
			searchSessionId:searchSessionId,
			lastQuestionId:lastQuestionId,
			signQuestionId:signQuestionId,
			extendQuestion:extendQuestion
		},function(data){
			if(data && data.success){
				$("#queryLogGrid").datagrid("reload");
			}else{
				$.messager.alert('警告','扩展问添加失败'); 
			}
		},"json");
	},
	//搜索轨迹
	searchTrack:{
		showTrackWin:function(searchSessionId){
			this.doQuery(searchSessionId);
			$("#searchTrackWin").window('open');
		},
		doQuery:function(searchSessionId){
			$.post("queryLog.do",{
				methName:queryLog.callMeth.searchTrackQuery,
				searchSessionId:searchSessionId
			},function(data){
				if(data && data.success){
					$("#searchTrackGrid").datagrid({
						data: data.rows
					});
				}
			},"json");
			
		}
	}
	
};

$(function(){
	queryLog.init();
});

function formatSearchTrack(val,row,index){  
    return '<a href="javascript:void(0);" onclick="queryLog.searchTrack.showTrackWin(\''+row.searchSessionId+'\')">' + val + '</a>';  
}

function formatOper(val,row,index){  
    return '<a href="javascript:void(0);" onclick="queryLog.ignore(\'' + row.searchSessionId + '\')">忽略</a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="queryLog.addInExpandQuestion(\''+row.searchSessionId+'\',\''+row.lastQuestionId+'\',\'' + row.signQuestionId + '\',\'' + row.searchContent + '\')">添加到扩展问</a>';  
}