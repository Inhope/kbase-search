
var newWordFind = {
		callMeth:{
			newWordQuery:"newWordQuery",
			ecludeNewWord:"ecludeNewWord",
			sureClassWord:"sureClassWord",
			getCategoryWords:"getCategoryWords",
			validSureNewWord:"validSureNewWord",
			findDetail:"findDetail"
		},
		//init
		init:function(){
			this.queryPanel.init();
			this.newWordTable.init();
		},
		//查询
		queryPanel:{
			sourceTypeVal:null,
			init:function(){
				this.bindEven();
			},
			//绑定事件
			bindEven:function(){
				var curObj = this;
				
				$("#sourceTypeSel").combobox({
					onSelect:function(rec){
						curObj.sourceTypeVal = rec.value;
					}
				})
				
				$('#searchBtn').bind('click', function() {
					curObj.doQuery();
				});
			},
			doQuery:function(){
				$("#newWordGrid").datagrid('load',{
					methName: newWordFind.callMeth.newWordQuery,
					sourceType: $("#sourceTypeSel").combobox("getValue"),
					createTimeStart: $("#createTimeStart").val(),
					createTimeEnd: $("#createTimeEnd").val()
				});
			}
		},
		newWordTable:{
			init:function(){
				this.loadCategoryData();
				this.bind();
				//禁止选中父节点
				$('#cateId').combotree({
				   onBeforeSelect : function(node) {
					    var rows = node.children;
					    if (rows.length > 0) {
					    	return false;
					    }
				   }
				});
			},
			bind:function(){
				var curO = this;
				$("#sureNewWordBtn").bind("click",function(){
					curO.addSureNewWord();
				});
			},
			loadCategoryData:function(){
				$.ajax({
					type:"POST",
					url:"newWord.do",
					data:{
						methName:newWordFind.callMeth.getCategoryWords
					},
					success:function(msg){
						if(msg && msg.success === true){
							$('#cateId').combotree({
								url:'resource/data/category.txt',
								method:'get'
							});
						}
					}
				});
			},
			showSureNewWordPanel:function(wordName){
				$("#addWin").window('open');
				$("#newWord").val(wordName);
				$("#synonymWord").val(wordName);
			},
			closeSureNewWordWin:function(){
				$("#addWin").window('close');
				$("#cateId").combotree("clear");
			},
			//添加(新词确认为合理新词)
			addSureNewWord:function(){
				var curO = this;
				var wordName = $("#newWord").val();
				//验证newword是否存在相似词
				$.ajax({
					type:"POST",
					url:"newWord.do",
					async:false,
					data:{
						methName:newWordFind.callMeth.validSureNewWord,
						wordName:wordName
					},
					success:function(msg){
						if(msg.success){
							curO.doAddSureNewWord(wordName);
						}else{
							$.messager.confirm('确认','有相似词类【' + msg.message + '】是否新增？',function(r){    
							    if (r){    
							    	curO.doAddSureNewWord(wordName);
							    }    
							});  
						}
					}
				});
			},
			doAddSureNewWord:function(wordName){
				var curObj = this;
				
				if(!this.validBeforAdd()){
					return;
				}
				
				$.ajax({
					type:"POST",
					url:"newWord.do",
					data:{
						methName:newWordFind.callMeth.sureClassWord,
						wordName:wordName,
						cateId:$("#cateId").combotree("getValue"),
						synonymWord:$("#synonymWord").val()
					},
					success:function(msg){
						if(msg && msg.success){
							curObj.closeSureNewWordWin();
							$("#newWordGrid").datagrid("reload");
						}
					}
				});
			},
			//return true：验证通过
			validBeforAdd:function(){
				//表单验证
				var	cateId = $("#cateId").combotree("getValue");
				if(!cateId){
					$.messager.alert('警告','新词分类为必填项！'); 
					return false;
				}
				
				return true;
			},
			//排除
			ecludeNewWord:function(wordName){
				var curObj = this;
				$.ajax({
					type:"POST",
					url:"newWord.do",
					data:{
						methName:newWordFind.callMeth.ecludeNewWord,
						wordName:wordName
					},
					success:function(msg){
						if(msg && msg.success){
							$("#newWordGrid").datagrid("reload");
						}
					}
				});
			}
		},
		//发现次数明细
		findDetail:{
			show:function(wordName){
				this.loadData(wordName);
				$("#detailWin").window('open');
			},
			loadData:function(wordName){
				$("#detailGrid").datagrid('load',{
					methName: newWordFind.callMeth.findDetail,
					wordName: wordName
				});
			}
		}
};

$(function(){
	newWordFind.init();
});


function formatOper(val,row,index){  
    return '<a href="javascript:void(0);" onclick="newWordFind.newWordTable.showSureNewWordPanel(\''+row.wordName+'\')">添加</a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="newWordFind.newWordTable.ecludeNewWord(\''+row.wordName+'\')">排除</a>';  
}

function formatFindCount(val,row,index){
	return '<a href="javascript:void(0);" onclick="newWordFind.findDetail.show(\''+row.wordName+'\')">' + val + '</a>';  
}