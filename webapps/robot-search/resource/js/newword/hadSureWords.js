var hadSureWords = {
		callMeth:{
			sureWordQuery:'sureWordQuery'
		},
		init:function(){
			this.bind();
		},
		bind:function(){
			var curO = this;
			$("#searchBtn").bind("click",function(){
				curO.doQuery();
			});
		},
		doQuery:function(){
			$("#hadSureGrid").datagrid('load',{
				methName: hadSureWords.callMeth.newWordQuery,
				sourceType: $("#sourceTypeSel").combobox("getValue"),
				createTimeStart: $("#createTimeStart").val(),
				createTimeEnd: $("#createTimeEnd").val()
			});
		}
};

$(function(){
	hadSureWords.init();
});

