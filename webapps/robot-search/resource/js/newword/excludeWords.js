var excludeWords = {
		callMeth:{
			ecludeWordQuery:'ecludeWordQuery',
			cancelExclude:'cancelExclude',
			sureClassWord:"sureClassWord",
			getCategoryWords:"getCategoryWords",
			validSureNewWord:"validSureNewWord"
		},
		init:function(){
			this.queryPanel.init();
			//禁止选中父节点
			$('#cateId').combotree({
			   onBeforeSelect : function(node) {
				    var rows = node.children;
				    if (rows.length > 0) {
				    	return false;
				    }
			   }
			});
		},
		//查询
		queryPanel:{
			init:function(){
				this.loadCategoryData();
				this.bind();
			},
			bind:function(){
				var curO = this;
				$("#searchBtn").bind("click",function(){
					curO.doQuery();
				});
				
				$("#sureNewWordBtn").bind("click",function(){
					curO.addSureNewWord();
				});
			},
			doQuery:function(){
				$("#excludeWordGrid").datagrid('load',{
					methName: excludeWords.callMeth.newWordQuery,
					sourceType: $("#sourceTypeSel").combobox("getValue"),
					createTimeStart: $("#createTimeStart").val(),
					createTimeEnd: $("#createTimeEnd").val(),
					createTimeStart: $("#excludeTimeStart").val(),
					createTimeEnd: $("#excludeTimeEnd").val()
				});
			},
			//解除
			cancelExclude:function(wordName){
				var curO = this;
				$.messager.confirm('确认','是否新增同义词？',function(r){    
				    if (r){    
				    	curO.showSureNewWordPanel(wordName);
				    }else{
				    	curO.doCancelExclude(wordName);
				    }    
				}); 
				
			},
			showSureNewWordPanel:function(wordName){
				$("#addWin").window('open');
				$("#newWord").val(wordName);
				$("#synonymWord").val(wordName);
			},
			closeSureNewWordWin:function(){
				$("#addWin").window('close');
				$("#cateId").combotree("clear");
			},
			doCancelExclude:function(wordName){
				$.ajax({
					type:"POST",
					url:"newWord.do",
					data:{
						methName:excludeWords.callMeth.cancelExclude,
						wordName:wordName
					},
					success:function(msg){
						if(msg && msg.success){
							$("#excludeWordGrid").datagrid("reload");
						}
					}
				});
			},
			//return true：验证通过
			validBeforAdd:function(){
				//表单验证
				var	cateId = $("#cateId").combotree("getValue");
				if(!cateId){
					$.messager.alert('警告','新词分类为必填项！'); 
					return false;
				}
				
				return true;
			},
			doAddSureNewWord:function(wordName){
				var curObj = this;
				
				if(!this.validBeforAdd()){
					return;
				}
				
				$.ajax({
					type:"POST",
					url:"newWord.do",
					data:{
						methName:excludeWords.callMeth.sureClassWord,
						wordName:wordName,
						cateId:$("#cateId").combotree("getValue"),
						synonymWord:$("#synonymWord").val()
					},
					success:function(msg){
						if(msg && msg.success){
							curObj.closeSureNewWordWin();
							$("#excludeWordGrid").datagrid("reload");
						}
					}
				});
			},
			//添加(新词确认为合理新词)
			addSureNewWord:function(){
				var curO = this;
				var wordName = $("#newWord").val();
				//验证newword是否存在相似词
				$.ajax({
					type:"POST",
					url:"newWord.do",
					async:false,
					data:{
						methName:excludeWords.callMeth.validSureNewWord,
						wordName:wordName
					},
					success:function(msg){
						if(msg.success){
							curO.doAddSureNewWord(wordName);
						}else{
							$.messager.confirm('确认','有相似词类【' + msg.message + '】是否新增？',function(r){    
							    if (r){    
							    	curO.doAddSureNewWord(wordName);
							    }    
							});  
						}
					}
				});
				
			},
			loadCategoryData:function(){
				$.ajax({
					type:"POST",
					url:"newWord.do",
					data:{
						methName:excludeWords.callMeth.getCategoryWords
					},
					success:function(msg){
						if(msg && msg.success === true){
							$('#cateId').combotree({
								url:'resource/data/category.txt',
								method:'get'
							});
						}
					}
				});
			}
		}
};

$(function(){
	excludeWords.init();
});


function formatOper(val,row,index){  
    return '<a href="javascript:void(0);" onclick="excludeWords.queryPanel.cancelExclude(\''+row.wordName+'\')">解除</a>';  
}