<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>新词发现</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="新词发现">
	
	<link rel="stylesheet" type="text/css" href="library/jquery-easyui-1.3.6/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="library/jquery-easyui-1.3.6/themes/icon.css">
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/jquery.min.js"></script>
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/locale/easyui-lang-zh_CN.js"></script>
  	<script type="text/javascript" src="library/My97DatePicker/WdatePicker.js"></script>
    <script src="resource/js/newword/newWordFind.js"></script>
  </head>
  
  <body>
  <div id="tb" style="padding:3px">
		<span>新词来源:</span>
		 <select id="sourceTypeSel" class="easyui-combobox" panelHeight="auto" style="width:100px">
			<option selected value="">选择新词来源</option>
			<option value="3">提问</option>
			<option value="1">知识</option>
			<option value="2">附件</option>
        </select>
		<span>发现日期:</span>
		<input id="createTimeStart" onclick="WdatePicker({maxDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})" value="" readonly="readonly" class="Wdate" type="text"  />
		--
		<input id="createTimeEnd" onclick="WdatePicker({minDate:'#F{$dp.$D(\'createTimeStart\')}',maxDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})"  readonly="readonly" class="Wdate" type="text"  />
		<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" id="searchBtn">查询</a>
	</div>
 	<table id="newWordGrid" class="easyui-datagrid" title="新词发现" style="height:500px"
			data-options="rownumbers:true,singleSelect:true,pagination:true,url:'newWord.do?methName=newWordQuery',method:'get'" toolbar="#tb">
		<thead>
			<tr>
					<th data-options="field:'wordName',width:120">新词名称</th>
					<th data-options="field:'createTime',width:80">发现日期</th>
					<th data-options="field:'sourceTypeStr',width:80">新词来源</th>
					<th data-options="field:'findCount',width:80,align:'right',formatter:formatFindCount,sortable:true">发现次数</th>
					<th data-options="field:'operateState',width:60,align:'center',formatter:formatOper">操作</th>
			</tr>
		</thead>
	</table>
	
<!-- 	发现明细面板 -->
	<div id="detailWin" class="easyui-window" title="发现明细" data-options="modal:true,closable:true,maximizable:false,minimizable:false,collapsible:false,closed:true"  style="width:820px;height:400px;">
		<table id="detailGrid" class="easyui-datagrid"   
				data-options="rownumbers:true,singleSelect:true,pagination:true,url:'newWord.do?methName=findDetail',method:'get'">
			<thead>
				<tr>
						<th data-options="field:'wordName',width:100">新词名称</th>
						<th data-options="field:'createTime',width:80">发现日期</th>
						<th data-options="field:'sourceTypeStr',width:80">新词来源</th>
						<th data-options="field:'sourceContent',width:400">新词语句</th>
				</tr>
			</thead>
		</table>
	</div>
	
<!-- 	添加新词 -->
	<div id="addWin" class="easyui-window" title="新增新词" data-options="modal:true,closable:false,maximizable:false,minimizable:false,collapsible:false,closed:true"  style="width:450px;height:300px;padding:10px;">
		<form id="addForm" method="post">
	    	<table cellpadding="3">
	    		<tr>
	    			<td>新词名称:</td>
	    			<td><input  name="newWord" id="newWord" readonly="readonly" style="width:200px;"></input></td>
	    		</tr>
	    		<tr>
	    			<td>新词分类 <span style="color:red;">*</span>:</td>
	    			<td>
	    				<select name="cateId" id="cateId" class="easyui-combotree" style="width:200px;"/>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>同义词:</td>
	    			<td>
	    				<textarea rows="3" cols="25" name="synonymWord" id="synonymWord"></textarea>
	    			</td>
	    		</tr>
	    	</table>
    	</form>
    	<div style="text-align:center;padding:5px">
	    	<a href="javascript:void(0)" class="easyui-linkbutton" id="sureNewWordBtn">保存</a>
	    	&nbsp;&nbsp;
	    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="newWordFind.newWordTable.closeSureNewWordWin()">取消</a>
	    </div>
	</div>
  </body>
</html>
