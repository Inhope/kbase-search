<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>已添加同义词</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="已添加同义词">
	<link rel="stylesheet" type="text/css" href="library/jquery-easyui-1.3.6/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="library/jquery-easyui-1.3.6/themes/icon.css">
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/jquery.min.js"></script>
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/jquery.easyui.min.js"></script>
 	<script type="text/javascript" src="library/jquery-easyui-1.3.6/locale/easyui-lang-zh_CN.js"></script>
 	<script type="text/javascript" src="library/My97DatePicker/WdatePicker.js"></script>
 	<script src="resource/js/newword/hadSureWords.js"></script>
  </head>
  
  <body>
     <div id="tb" style="padding:3px">
		<span>新词来源:</span>
		 <select id="sourceTypeSel" class="easyui-combobox" panelHeight="auto" style="width:100px">
			<option selected value="">选择新词来源</option>
			<option value="3">提问</option>
			<option value="1">知识</option>
			<option value="2">附件</option>
        </select>
		<span>发现日期:</span>
		<input id="createTimeStart" onclick="WdatePicker({maxDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})"  readonly="readonly" class="Wdate" type="text"  />
		--
		<input id="createTimeEnd" onclick="WdatePicker({minDate:'#F{$dp.$D(\'createTimeStart\')}',maxDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})"  readonly="readonly" class="Wdate" type="text"  />
		<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" id="searchBtn">查询</a>
	</div>
 	<table id="hadSureGrid" class="easyui-datagrid" title="同义词添加" style="height:500px"
			data-options="rownumbers:true,singleSelect:true,pagination:true,url:'newWord.do?methName=sureWordQuery',method:'get'" toolbar="#tb">
		<thead>
			<tr>
					<th data-options="field:'wordName',width:100">新词名称</th>
					<th data-options="field:'createTime',width:80">发现日期</th>
					<th data-options="field:'insertTime',width:80">添加日期</th>
					<th data-options="field:'categoryName',width:80">新词分类</th>
					<th data-options="field:'synonymWord',width:180">同义词</th>
			</tr>
		</thead>
	</table>
  </body>
</html>
