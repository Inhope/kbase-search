<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>配置刷新</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="新词发现">
	
	<link rel="stylesheet" type="text/css" href="library/jquery-easyui-1.3.6/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="library/jquery-easyui-1.3.6/themes/icon.css">
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/jquery.min.js"></script>
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/locale/easyui-lang-zh_CN.js"></script>
  </head>
  <script type="text/javascript">
	 $(function(){
		 //刷新搜索模板配置
		  $("#temp").click(function(){
			 $.ajax({
				 url:"flush.do",
				 data:{
					 flushProperty:"1"
				 },
				 success:function(date){
					 alert(date);
				 }
			 }); 
		  });
	 });
  </script>
  <body>
  	<button id="temp" >刷新搜索模板配置</button>
  </body>
</html>
