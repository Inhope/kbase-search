<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>搜索日志列表</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="新词发现">
	
	<link rel="stylesheet" type="text/css" href="library/jquery-easyui-1.3.6/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="library/jquery-easyui-1.3.6/themes/icon.css">
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/jquery.min.js"></script>
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="library/jquery-easyui-1.3.6/locale/easyui-lang-zh_CN.js"></script>
  	<script type="text/javascript" src="library/My97DatePicker/WdatePicker.js"></script>
    <script src="resource/js/olog/queryLog.js"></script>
  </head>
  
  <body>
  <div id="tb" style="padding:3px">
		<span>提问日期:</span>
		<input id="tSearchDataStart" onclick="WdatePicker({maxDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})" value="" readonly="readonly" class="Wdate" type="text"  />
		--
		<input id="tSearchDataEnd" onclick="WdatePicker({minDate:'#F{$dp.$D(\'tSearchDataStart\')}',maxDate:'%y-%M-%d',dateFmt:'yyyy-MM-dd'})"  readonly="readonly" class="Wdate" type="text"  />
		<span>搜索轨迹:</span>
		 <select id="tSearchTrackCountOneExpr" class="easyui-combobox" panelHeight="auto" style="width:50px">
			<option selected value="=">=</option>
			<option value="&gt;">&gt;</option>
			<option value="&lt;">&lt;</option>
        </select>
        <input id="tSearchTrackCountOne" type="text" class="easyui-numberbox" data-options="min:0" style="width:40px;"></input>
        --
        <select id="tSearchTrackCountTwoExpr" class="easyui-combobox" panelHeight="auto" style="width:50px">
			<option selected value="=">=</option>
			<option value="&gt;">&gt;</option>
			<option value="&lt;">&lt;</option>
        </select>
        <input id="tSearchTrackCountTwo" type="text" class="easyui-numberbox" data-options="min:0" style="width:40px;"></input>
		<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" id="searchBtn">查询</a>
		<span style="float:right;" id="signRatio"></span>
	</div>
 	<table id="queryLogGrid" class="easyui-datagrid" title="搜索日志" style="height:500px"
			data-options="rownumbers:true,singleSelect:true,pagination:true,url:'queryLog.do?methName=doQuery',method:'get'" toolbar="#tb">
		<thead>
			<tr>
				<th data-options="field:'searchContent',width:200">提问文本</th>
				<th data-options="field:'searchDate',width:80">提问日期</th>
				<th data-options="field:'lastQuestion',width:200">最后访问知识名称</th>
				<th data-options="field:'signQuestion',width:200">点击解决知识名称</th>
				<th data-options="field:'pageNo',width:80,align:'right'">第几页</th>
				<th data-options="field:'pageSequence',width:80,align:'right'">第几条</th>
				<th data-options="field:'searchTrackCount',width:80,align:'right',formatter:formatSearchTrack">搜索轨迹</th>
				<th data-options="field:'operateState',width:120,align:'center',formatter:formatOper">操作</th>
			</tr>
		</thead>
	</table>
	
<!-- 	搜索轨迹 -->
	<div id="searchTrackWin" class="easyui-window" title="搜索轨迹" data-options="modal:true,closable:true,maximizable:false,minimizable:false,collapsible:false,closed:true"  style="width:620px;height:400px;">
		<table id="searchTrackGrid" class="easyui-datagrid"   
				data-options="rownumbers:true,singleSelect:true">
			<thead>
				<tr>
					<th data-options="field:'question',width:500">标准问</th>
				</tr>
			</thead>
		</table>
	</div>
  </body>
</html>
